class Scrambleizer {
  // TODO:
  // - handle more endmarks => _ + / etc.
  // - handle startmarks => currency, etc.
  static String transform(String inputString) {
    // "" = 8220, 8221
    // '' = 8216, 8217
    // () = 40, 41
    // [] = 91, 93
    // {} = 123,125
    // <> = 60, 62
    List<int> openingMarkCodeUnits = [8220, 8216, 39, 40, 91, 123, 60];
    List<int> closingMarkCodeUnits = [8221, 8217, 39, 41, 93, 125, 62];
    List<int> endMarkCodeUnits = [46, 44, 58, 59, 45, 33, 63]; // . , : ; - ! ?
    int apostropheCodeUnit = 8217;

    // convert word into codeUnits
    List<int> codeUnitList = inputString.codeUnits;
    List<int> inputStringCodeUnits = [];
    codeUnitList.forEach((element) => inputStringCodeUnits.add(element));

    // debugPrint('inputStringCodeUnits start: $inputStringCodeUnits');

    int openingMarkCodeUnit;
    int closingMarkCodeUnit;
    int endMarkCodeUnit;
    List<int> itemsAfterApostrophe;
    int firstLetterCodeUnit;
    int lastLetterCodeUnit;

    // 1. check if first and last characters are the same (surroundMark)
    if (openingMarkCodeUnits.contains(inputStringCodeUnits[0]) &&
        closingMarkCodeUnits
            .contains(inputStringCodeUnits[inputStringCodeUnits.length - 1])) {
      // if they are, remove and store them as 1
      openingMarkCodeUnit = inputStringCodeUnits.removeAt(0);
      closingMarkCodeUnit = inputStringCodeUnits.removeLast();
    }

    // 2. check if remainder has an end mark (endMark)
    if (endMarkCodeUnits
        .contains(inputStringCodeUnits[inputStringCodeUnits.length - 1])) {
      endMarkCodeUnit = inputStringCodeUnits.removeLast();
    }

    // 3. check if remainder has an apostrophe
    if (inputStringCodeUnits.contains(apostropheCodeUnit)) {
      // find index of apostrophe
      int apIndex =
          inputStringCodeUnits.indexWhere((e) => e == apostropheCodeUnit);

      // save items after apostrophe and remove from inputString
      itemsAfterApostrophe = inputStringCodeUnits.sublist(apIndex);
      inputStringCodeUnits.removeRange(apIndex, inputStringCodeUnits.length);

      // debugPrint('itemsAfterApostrophe $itemsAfterApostrophe');
    }

    // debugPrint('inputStringCodeUnits to process $inputStringCodeUnits');

    // 4. remove first and last array items => has to be more than one
    if (inputStringCodeUnits.length > 1) {
      firstLetterCodeUnit = inputStringCodeUnits.removeAt(0);
      lastLetterCodeUnit = inputStringCodeUnits.removeLast();
    }

    // shuffle code unit array (reverse 2 member lists)
    if (inputStringCodeUnits.length == 2)
      inputStringCodeUnits = inputStringCodeUnits.reversed.toList();
    else
      inputStringCodeUnits.shuffle();

    // add removed items 4
    if (firstLetterCodeUnit != null && lastLetterCodeUnit != null) {
      inputStringCodeUnits.insert(0, firstLetterCodeUnit);
      inputStringCodeUnits.add(lastLetterCodeUnit);
    }

    // add removed items 3
    if (itemsAfterApostrophe != null)
      inputStringCodeUnits.addAll(itemsAfterApostrophe);

    // add removed items 2
    if (endMarkCodeUnit != null) inputStringCodeUnits.add(endMarkCodeUnit);

    // add removed items 1
    if (openingMarkCodeUnit != null && closingMarkCodeUnit != null) {
      inputStringCodeUnits.insert(0, openingMarkCodeUnit);
      inputStringCodeUnits.add(closingMarkCodeUnit);
    }
    // convert back to string
    return String.fromCharCodes(inputStringCodeUnits);
  }
}
