class Reversify {
  static String transform(String inputString) {
    return inputString.split('').reversed.join('');
  }
}
