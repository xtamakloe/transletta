class Spaceify {
  static String transform(String inputString) {
    return inputString.split('').join(' ');
  }
}
