class Alternatify {
  static String transform(String inputString) {
    List<String> result = [];
    List<String> inputArray = inputString.split('');

    for (var i = 0; i < inputArray.length; i++) {
      result.add(
          i.isEven ? inputArray[i].toUpperCase() : inputArray[i].toLowerCase());
    }
    return result.join('');
  }
}
