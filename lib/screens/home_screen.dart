import 'dart:core';

import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:share/share.dart';
import 'package:swipe_gesture_recognizer/swipe_gesture_recognizer.dart';
import 'package:transletta/screens/help_screen.dart';
import 'package:transletta/util/alternatify.dart';
import 'package:transletta/util/capsify.dart';
import 'package:transletta/util/dotify.dart';
import 'package:transletta/util/reversify.dart';
import 'package:transletta/util/scrambleizer.dart';
import 'package:transletta/util/spaceify.dart';
import 'package:transletta/util/titleizer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

enum Format {
  scrambleCase,
  titleCase,
  reversify,
  capsify,
  alternatify,
  spaceify,
  dotify,
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _controller = TextEditingController();

  String _text = '';
  int _formatIndex = 0;
  List<Format> _formats = [
    Format.scrambleCase,
    Format.titleCase,
    Format.reversify,
    Format.capsify,
    Format.alternatify,
    Format.spaceify,
    Format.dotify,
  ];
  List<String> _buttonLabels = [
    'Scrambleize',
    'Titleize',
    'Reversify',
    'Capsify',
    'Alternatify',
    'Spaceify',
    'Dotify',
  ];

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return Container(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Builder(
          builder: (context) => SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // HEADER
                Expanded(
                  flex: 1,
                  child: Row(
                    children: [
                      Expanded(
                        // flex: 10,
                        child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            child: Text(
                              'transletta.',
                              textAlign: TextAlign.center,
                              style: textStyle(
                                  color: Colors.white,
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.bold),
                            )),
                      ),
                      InkWell(
                        onTap: () => showCupertinoModalBottomSheet(
                            expand: false,
                            context: context,
                            backgroundColor: Colors.transparent,
                            builder: (context, scrollController) =>
                                HelpScreen()),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.help,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                // RESULTS VIEW
                Expanded(
                  flex: 3,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () => _copyText(context),
                          onDoubleTap: () => _shareText(),
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(color: Colors.black),
                            height: MediaQuery.of(context).size.height * .5,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 25.0, horizontal: 25.0),
                              child: SingleChildScrollView(
                                child: Text(
                                  _text,
                                  textAlign: TextAlign.start,
                                  style: textStyle(
                                      color: Colors.white, fontSize: 18.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // TEXTFIELD
                Expanded(
                  flex: 4,
                  child: Container(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 25.0, vertical: 20.0),
                      child: Container(
                        height: MediaQuery.of(context).size.height * .25,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.black, width: 2.0),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: TextField(
                                  keyboardType: TextInputType.multiline,
                                  controller: _controller,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: textStyle(color: Colors.grey),
                                    labelStyle: textStyle(color: Colors.grey),
                                    hintText: 'Enter message here...',
                                    focusColor: Colors.white,
                                    fillColor: Colors.white,
                                  ),
                                  style: textStyle(color: Colors.black),
                                  autocorrect: true,
                                  showCursor: true,
                                  cursorColor: Colors.black,
                                  maxLines: null, // wrap input
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Align(
                                  alignment: Alignment.bottomRight,
                                  child: InkWell(
                                    onTap: () => _pasteFromClipboard(),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: SizedBox(
                                          height: 25.0,
                                          child: Image.asset(
                                              'assets/images/paste.png')),
                                    ),
                                  )),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                // BUTTON

                Expanded(
                  flex: 2,
                  child: Container(
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SwipeGestureRecognizer(
                          onSwipeLeft: () => _reset(),
                          onSwipeUp: () => _gotoNextMode(),
                          onSwipeDown: () => _gotoPreviousMode(),
                          child: InkWell(
                            onTap: () => _transformText(),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width * .85,
                              height: 58.0,
                              child: Text(
                                _buttonLabels[_formatIndex],
                                style: textStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _transformText() {
    if (_controller.text.isEmpty) return;

    List<String> results = [];

    _controller.text.trim().split(' ').forEach((inputString) {
      results.add(isTransformable(inputString)
          ? _transform(inputString.trim())
          : removeDelimiters(inputString));
    });

    setState(() => _text = results.join(' '));
  }

  _transform(String word) {
    String results;

    switch (_formats[_formatIndex]) {
      case Format.titleCase:
        results = Titleizer.transform(word);
        break;
      case Format.scrambleCase:
        results = Scrambleizer.transform(word);
        break;
      case Format.reversify:
        results = Reversify.transform(word);
        break;
      case Format.capsify:
        results = Capsify.transform(word);
        break;
      case Format.alternatify:
        results = Alternatify.transform(word);
        break;
      case Format.spaceify:
        results = Spaceify.transform(word);
        break;
      case Format.dotify:
        results = Dotify.transform(word);
        break;
      default:
        results = word;
        break;
    }
    return results;
  }

  bool isTransformable(String word) {
    // Words not surrounded by delimiter(*) are scrambleable
    return !RegExp(r'^\*.*\*$').hasMatch(word); // delimiter => \*
  }

// Remove delimiters that direct word to not be modified
  String removeDelimiters(String word) {
    return word.replaceAll(RegExp(r'\*'), '');
  }

  textStyle({Color color, double fontSize, FontWeight fontWeight}) {
    return GoogleFonts.sourceCodePro(
        textStyle: TextStyle(
            fontSize: fontSize, color: color, fontWeight: fontWeight));
  }

  _textFieldBorder() {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: BorderSide(color: Colors.black, width: 2.0));
  }

  _copyText(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());

    if (_text.isNotEmpty) {
      Clipboard.setData(ClipboardData(text: _text));
      _showAlert(context, Colors.white, 'Copied!');
    }
  }

  _shareText() {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_text.isNotEmpty) Share.share(_text);
  }

  _showAlert(BuildContext context, Color color, String text) {
    Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 2),
        backgroundColor: color,
        content: Text(text,
            textAlign: TextAlign.center,
            style:
                textStyle(color: Colors.black, fontWeight: FontWeight.bold))));
  }

  _reset() {
    setState(() {
      _text = '';
      _controller.text = '';
    });
  }

  _gotoNextMode() {
    setState(() =>
        _formatIndex < _formats.length - 1 ? _formatIndex++ : _formatIndex = 0);
  }

  _gotoPreviousMode() {
    setState(() => _formatIndex == 0
        ? _formatIndex = _formats.length - 1
        : _formatIndex--);
  }

  _pasteFromClipboard() {
    FlutterClipboard.paste()
        .then((value) => setState(() => _controller.text = value));
  }
}
