import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HelpScreen extends StatelessWidget {
  double iconHeight = 35.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Row(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                            color: Colors.black,
                            alignment: Alignment.center,
                            // width: MediaQuery.of(context).size.width,
                            child: Text(
                              'how-to.',
                              textAlign: TextAlign.center,
                              style: _textStyle(
                                  color: Colors.white,
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.bold),
                            )),
                      ),
                      Container(
                        color: Colors.black,
                        child: Column(
                          children: [
                            InkWell(
                              onTap: () => Navigator.pop(context),
                              child: Icon(
                                Icons.cancel,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 3,
              child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    Flexible(
                      flex: 2,
                      child: Row(
                        children: [
                          SizedBox(
                              height: iconHeight,
                              child: Image.asset('assets/images/swipe-up.png')),
                          SizedBox(
                            height: iconHeight,
                            child: Image.asset('assets/images/swipe-down.png'),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      flex: 6,
                      child: Text(
                        'Swipe up or down on button to change modes.',
                        style: _textStyle(fontSize: 14.0, color: Colors.black),
                      ),
                    )
                  ],
                ),
                Row(
                  children: [
                    Flexible(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(
                                height: iconHeight,
                                child: Image.asset(
                                    'assets/images/swipe-left.png')),
                          ],
                        )),
                    SizedBox(
                      width: 10.0,
                    ),
                    Flexible(
                        flex: 6,
                        child: Text(
                          'Swipe left on button to clear.',
                          style:
                              _textStyle(fontSize: 14.0, color: Colors.black),
                        ))
                  ],
                ),

                Row(
                  children: [
                    Flexible(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(
                                height: iconHeight,
                                child: Image.asset('assets/images/tap.png')),
                          ],
                        )),
                    SizedBox(
                      width: 10.0,
                    ),
                    Flexible(
                        flex: 6,
                        child: Text('Tap results to copy.',
                            style: _textStyle(
                                fontSize: 14.0, color: Colors.black)))
                  ],
                ),

                Row(
                  children: [
                    Flexible(
                      flex: 2,
                      child: Row(
                        children: [
                          SizedBox(
                              height: iconHeight,
                              child: Image.asset('assets/images/tap.png')),
                          SizedBox(
                              height: iconHeight,
                              child: Image.asset('assets/images/tap.png')),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Flexible(
                        flex: 6,
                        child: Text('Double tap results to share.',
                            style: _textStyle(
                                fontSize: 14.0, color: Colors.black)))
                  ],
                ),

                Row(
                  children: [
                    Flexible(
                      flex: 2,
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                                height: 30.0,
                                child: Image.asset('assets/images/paste.png')),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width:
                      10.0,
                    ),
                    Flexible(
                        flex: 6,
                        child: Text('Tap icon to paste from clipboard.',
                            style: _textStyle(
                                fontSize: 14.0, color: Colors.black)))
                  ],
                ),

                Row(
                  children: [
                    Flexible(
                      flex: 2,
                      child: Row(
                        children: [
                          Text('*Laura*', style: _textStyle(fontSize: 14.0)),
                        ],
                      ),

                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Flexible(
                        flex: 6,
                        child: Text(
                            'Surround word with asterisks (*) to ignore it.',
                            style: _textStyle(
                                fontSize: 14.0, color: Colors.black)))
                  ],
                ),
              ],
            ),
          )),
        ],
      ),
    );
  }

  _textStyle({Color color, double fontSize, FontWeight fontWeight}) {
    return GoogleFonts.sourceCodePro(
        textStyle: TextStyle(
            fontSize: fontSize, color: color, fontWeight: fontWeight));
  }
}
